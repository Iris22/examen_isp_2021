package sub2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class Window extends JFrame{
    JTextField textField1, textField2;
    JButton button;

    Window(){
        setTitle("Number of characters");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,300);
        setVisible(true);
    }

    public void init(){
        this.setLayout(null);
        int width=160;int height = 20;

        textField1 = new JTextField();
        textField1.setBounds(10,50,width,height);
        add(textField1);

        textField2 = new JTextField();
        textField2.setBounds(50,150,width/2,height);
        add(textField2);

        button = new JButton("Get number");
        button.setBounds(10,100,width,height);
        add(button);
        button.addActionListener(new ButtonPress());
    }

    public static void main(String[] args) {
        new Window();
    }

    class ButtonPress implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            String textF = Window.this.textField1.getText();
            try {
                File file = new File("src\\sub2\\textF");
                FileInputStream fileStream = new FileInputStream(file);
                InputStreamReader input = new InputStreamReader(fileStream);
                BufferedReader reader = new BufferedReader(input);

                String line;

                int characterCount = 0;
                int countWord = 0;
                int spaceCount = 0;
                int paragraphCount = 1;

                // Reading line by line from the
                // file until a null is returned
                while ((line = reader.readLine()) != null) {
                    if (line.equals("")) {
                        paragraphCount++;
                    } else {
                        characterCount += line.length();

                        // \\s+ is the space delimiter in java
                        String[] wordList = line.split("\\s+");
                        countWord += wordList.length;
                        spaceCount += countWord - 1;
                    }
                }
                characterCount = characterCount - spaceCount;

                input.close();
                String s = String.valueOf(characterCount);
                Window.this.textField2.setText(s);
            }
            catch (FileNotFoundException ex){
                System.out.println("File does not exist");
                ex.printStackTrace();
            } catch (IOException ex){
                ex.printStackTrace();
            }

        }
    }
}
