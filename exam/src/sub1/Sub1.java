package sub1;

public class Sub1 {
    public class A{

    }

    public class B extends A{
        private long t;

        public void b(C c){
            System.out.println(c.getText());
        }
    }

    public class C{
        public String getText(){
            return "text";
        }
    }

    public class D{
        private B b;
        E e = new E();
    }

    public class E{

    }

    public class F{
        private D d = new D();
    }
}
